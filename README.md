# A Practical Guide to Setting Up Tensorflow on Darwin Clusters

Step 1. Install [Anaconda](https://www.continuum.io/downloads).
   I used _Python2.7_ version. 

Step 2. Install libgcc in conda
``` python
conda install libgcc
```

Step 3. Check if *GLIBC_2.14* is present. This is requried by tensorflow-r0.10.
   I encountered a problem when installing the latest version of tensorflow, so I rolled back to r0.10.
``` shell
strings /lib64/libc.so.6 | grep GLIBC
```
   If NOT install a local copy. Download [glibc-2.14](https://ftp.gnu.org/gnu/libc/) and follow the instructions below:
``` shell
[root@localhost ~]# tar xvf glibc-2.14.tar.gz
[root@localhost ~]# cd glibc-2.14
[root@localhost glibc-2.14]# mkdir build
[root@localhost glibc-2.14]# cd ./build
[root@localhost build]# sudo ../configure --prefix=/opt/glibc-2.14
[root@localhost build]# sudo make -j4
[root@localhost build]# sudo make install
```
  *Attention* Remember to modify your bashrc!
``` shell
export LD_LIBRARY_PATH=/opt/glibc-2.14/lib:$LD_LIBRARY_PATH
```

Step 4. Install tensorflow-r0.10 in Anaconda.
``` shell
# pay attention to the python version here, which should be match the version of tf
conda create -n tensorflow python=2.7

source activate tensorflow

# this is the version of r0.10, and you probably CANNOT find it on the official TF website now, but it is still accessible.
export TF_BINARY_URL=https://storage.googleapis.com/tensorflow/linux/cpu/tensorflow-0.10.0-cp27-none-linux_x86_64.whl

# Python 2
(tensorflow)$ pip install --ignore-installed --upgrade $TF_BINARY_URL
```

I'd suggest you use the latest version of tensorflow. But due to the old version of glibc on Darwin, you would probably 
need to install a local copy of GLIBC with higher version than 2.14.

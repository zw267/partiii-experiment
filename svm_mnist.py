import matplotlib.pyplot as plt
import numpy as np
import time
import datetime as dt

from sklearn import datasets, svm, metrics
from sklearn.datasets import fetch_mldata

from tensorflow.examples.tutorials.mnist import input_data

mnist = input_data.read_data_sets("/home/zw267/scratch/MNIST_data/", one_hot=False)

X_train = mnist.train.images
y_train = mnist.train.labels

X_test = mnist.test.images
y_test = mnist.test.labels


print y_train

param_C = 5
param_gamma = 0.05
classifier = svm.SVC(C=param_C,gamma=param_gamma)

# We learn the digits on train part
start_time = dt.datetime.now()
print('Start learning at {}'.format(str(start_time)))
classifier.fit(X_train, y_train)
end_time = dt.datetime.now() 
print('Stop learning {}'.format(str(end_time)))
elapsed_time= end_time - start_time
print('Elapsed learning {}'.format(str(elapsed_time)))

print('number of support vectors {}:'.format(str(classifier.n_support_)))


########################################################




# Now predict the value of the test
expected = y_test
start_time = dt.datetime.now()
print('Start testing at {}'.format(str(start_time)))
predicted = classifier.predict(X_test)

end_time = dt.datetime.now() 
print('Stop testing {}'.format(str(end_time)))
elapsed_time= end_time - start_time
print('Elapsed testing {}'.format(str(elapsed_time)))

print("Classification report for classifier %s:\n%s\n"
      % (classifier, metrics.classification_report(expected, predicted)))
      

print("Accuracy={}".format(metrics.accuracy_score(expected, predicted)))



# Standard scientific Python imports
import matplotlib.pyplot as plt
import numpy as np
import time
import datetime as dt

from sklearn.neighbors import KNeighborsClassifier

# Import datasets, classifiers and performance metrics
from sklearn import datasets, svm, metrics
#fetch original mnist dataset
from sklearn.datasets import fetch_mldata


from tensorflow.examples.tutorials.mnist import input_data

#mnist = input_data.read_data_sets("/home/zw267/scratch/MNIST_data/", one_hot=True)
mnist = input_data.read_data_sets("/home/zw267/scratch/MNIST_data/", one_hot=False)
#mnist = fetch_mldata('MNIST original', data_home='/home/zw267/scratch/')

#minist object contains: data, COL_NAMES, DESCR, target fields
#you can check it by running
#mnist.keys()

#data field is 70k x 784 array, each row represents pixels from 28x28=784 image
#images = mnist.data
#targets = mnist.target

# Let's have a look at the random 16 images, 
# We have to reshape each data row, from flat array of 784 int to 28x28 2D array

#pick  random indexes from 0 to size of our dataset
#show_some_digits(images,targets)

#---------------- classification begins -----------------
#scale data for [0,255] -> [0,1]
#sample smaller size for testing
#rand_idx = np.random.choice(images.shape[0],10000)
#X_data =images[rand_idx]/255.0
#Y      = targets[rand_idx]

#full dataset classification
#X_data = images/255.0
#X_data = mnist.train.images
#Y = targets
#Y = mnist.train.labels

#split data to train and test
#from sklearn.cross_validation import train_test_split
#from sklearn.model_selection import train_test_split
#X_train, X_test, y_train, y_test = train_test_split(X_data, Y, test_size=0.15, random_state=42)
X_train = mnist.train.images
y_train = mnist.train.labels

X_test = mnist.test.images
y_test = mnist.test.labels

start_time = dt.datetime.now()
print('Start learning at {}'.format(str(start_time)))

clf = KNeighborsClassifier(algorithm="kd_tree")
clf.fit(X_train, y_train)


end_time = dt.datetime.now() 
print('Stop learning {}'.format(str(end_time)))
elapsed_time= end_time - start_time
print('Elapsed learning {}'.format(str(elapsed_time)))


########################################################




# Now predict the value of the test

start_time = dt.datetime.now()
print('Start learning at {}'.format(str(start_time)))
predicted = clf.predict(X_test)
print("Accuracy={}".format(metrics.accuracy_score(y_test, predicted)))

end_time = dt.datetime.now() 
print('Stop learning {}'.format(str(end_time)))
elapsed_time= end_time - start_time
print('Elapsed learning {}'.format(str(elapsed_time)))
